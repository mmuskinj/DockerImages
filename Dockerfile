FROM atlas/atlas_external_cvmfs
MAINTAINER miha.muskinja@cern.ch
USER root

RUN yum -y install man uuid uuid-devel libuuid-devel xrootd-client && \
    yum -y clean all